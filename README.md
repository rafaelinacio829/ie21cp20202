# Introdução
Este projeto tem como objetivo a criação de switch com relé, para automação, controladas por controles IR(Infravermelho), com display para a identificação de modos pré programados para o usuário escolher qual configuração desejada gostaria de aplicar.
o projeto pode ser aplicado em diversas formas e áreas de smarthomes, tais como: Lampadas, sensores de segurança, eletrodomésticos.
## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Jeferson Lima|jefersonlima|
|Rafael Inácio|rafaelinacio829|
|Satil Pereira|satil_pereira|
|Fernando Gnoatto|fernando_gnoatto|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> ...

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

